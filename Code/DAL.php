<?php

include realpath(dirname(__FILE__)) . '/Command.php';
include realpath(dirname(__FILE__)) . '/Utils.php';
include realpath(dirname(__FILE__)) . '/Device.php';
include realpath(dirname(__FILE__)) . '/Parameters.php';
require realpath(dirname(__FILE__)) . '/../vendor/autoload.php';

class DAL {
    /* @var $client Aws\DynamoDb\DynamoDbClient */

    public static $client = null;
    /* @var $dynamodb Aws\DynamoDb\DynamoDbClient */
    public static $dynamodb = null;

    const COMMANDS_TABLE = "commands";
    const DEVICES_TABLE = "devices_main";
    const EXTRA_IMEI_INDEX = "ExtraIMEI-index";
    const IMEI_INDEX = "IMEI-index";
    const GROUPS_TABLE = "groups";

    function __construct() {
        try {
            $val = true;
            if (self::$client == null || self::$dynamodb == null) {
                self::$client = \Aws\DynamoDb\DynamoDbClient::factory(array(
                            'credentials' => array(
                                'key' => 'AKIAJGPR4ZJR2TLTAY6Q',
                                'secret' => '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',
                            ),
                            'region' => 'ap-south-1',
                            'version' => 'latest'
                ));
                $sdk = new \Aws\Sdk(array(
                    'credentials' => array(
                        'key' => 'AKIAJGPR4ZJR2TLTAY6Q',
                        'secret' => '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',
                    ),
                    'region' => 'ap-south-1',
                    'version' => 'latest'
                ));
                self::$dynamodb = $sdk->createDynamoDb();
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function UpdateDeviceTable() {
        try {
            $ch = curl_init(Parameters::UPDATE_TABLE_URL);
            curl_setopt($ch, CURLOPT_URL, Parameters::UPDATE_TABLE_URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
            return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function GetAllDevices() {
        $iterator = self::$client->getIterator('Scan', array(
            'TableName' => self::DEVICES_TABLE
        ));

        $allDevices = array();
        foreach ($iterator as $item) {
            array_push($allDevices, Device::FromItem($item));
        }

        return $allDevices;
    }

    function GetAllCommands() {
        $iterator = self::$client->getIterator('Scan', array(
            'TableName' => self::COMMANDS_TABLE
        ));

        $commands = array();
        foreach ($iterator as $item) {
            array_push($commands, Command::FromItem($item));
        }

        return $commands;
    }

    function GetAllGroups() {

        $iterator = self::$client->getIterator('Scan', array(
            'TableName' => self::GROUPS_TABLE
        ));

        $groupsAll = array();
        foreach ($iterator as $item) {
            array_push($groupsAll, $item['GroupName']['S']);
        }
        $groupsAll = array_unique($groupsAll);

        return $groupsAll;
    }

    public function DeviceExists($device, $SecondaryInex = false) {
        try {
            $response;
            if (!$SecondaryInex) {
                $response = self::$dynamodb->query([
                    'TableName' => self::DEVICES_TABLE,
                    'KeyConditionExpression' => 'FunkeyId = :v_id',
                    'ExpressionAttributeValues' => [
                        ':v_id' => ['S' => $device]
                    ]
                ]);
            } else {
                $primKey;
                if ($SecondaryInex == self::IMEI_INDEX) {
                    $primKey = 'IMEI';
                } else if ($SecondaryInex == self::EXTRA_IMEI_INDEX) {
                    $primKey = "ExtraIMEI";
                }
                $response = self::$dynamodb->query([
                    'TableName' => self::DEVICES_TABLE,
                    'IndexName' => $SecondaryInex,
                    'KeyConditionExpression' => '#id = :v_dt',
                    'ExpressionAttributeNames' => ['#id' => $primKey],
                    'ExpressionAttributeValues' => [
                        ':v_dt' => ['S' => $device],
                    ],
                    'ScanIndexForward' => true,
                ]);
            }
            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] != 200) {
                return $var["@metadata"]["statusCode"];
            }
            $items = $response["Items"];
            if (count($items) == 1) {
                return TRUE;
            }
            return FALSE;
        } catch (Exception $exc) {
            echo $exc->getMessage();
            return FALSE;
        }
    }

    public function AddGroupsToCommands($commands, $groups) {
        foreach ($commands as $command) {
            $com = $this->GetCommandById($command);
            if ($com != FALSE) {
                if (property_exists($com, "Groups") && $com->Groups != null) {
                    $groups = array_merge($groups, $com->Groups);
                }
                $groups = array_unique($groups);
                $this->AddGroupsToCommand($command, $groups);
            } else {
                return "Command could not be found commandid: " . $command;
            }
        }
        return TRUE;
    }

    public function AddGroupsToCommand($command, $groups) {
        try {
            $response = self::$dynamodb->updateItem([
                'TableName' => self::COMMANDS_TABLE,
                'Key' => [
                    'CommandId' => [ 'S' => $command]
                ],
                'ExpressionAttributeNames' => [
                    '#grp' => 'Groups'
                ],
                'ExpressionAttributeValues' => [
                    ':val1' => ['SS' => $groups]
                ],
                'UpdateExpression' => 'ADD #grp :val1'
            ]);

            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] == 200) {
                return true;
            } else {
                return $var["@metadata"]["statusCode"];
            }
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }

    public function RemoveCommandsFromGroups($commands, $groups) {
        foreach ($commands as $command) {
            $com = $this->GetCommandById($command);
            if ($com != FALSE) {
                $this->RemoveGroupFromCommand($command, $groups);
            } else {
                return "Command could not be found commandid: " . $command;
            }
        }
        return true;
    }
    
    public function GetAutoRemoveCommandForGroup($group) {
        try {
            $params = [
                'TableName' => self::COMMANDS_TABLE,
                'FilterExpression' => 'contains (Groups, :v_Group) AND CommandName = :v_Name',
                'ExpressionAttributeValues' => [
                    ':v_Group' => ['S' => $group],
                    ':v_Name' => ['S' => Parameters::AUTO_GENERATED_REMOVE_COMMAND_NAME]
                ]
            ];
            $response = self::$dynamodb->scan($params);
            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] != 200) {
                return $var["@metadata"]["statusCode"];
            }
            $items = $response["Items"];
            if (count($items) == 1) {
                return Command::FromItem($items[0]);
            }
            return FALSE;
        } catch (Exception $exc) {
            // echo $exc->getTraceAsString();
            return FALSE;
        }
    }
    
    public function DeleteCommandById($id) {
        try {
            /* @var $response Aws\Result */
            $response = self::$dynamodb->deleteItem([
                'TableName' => self::COMMANDS_TABLE,
                'Key' => [
                    'CommandId' => ['S' => $id ]
                ]
            ]);

            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] == 200) {
                return TRUE;
            } else {
                return $var["@metadata"]["statusCode"];
            }
        } catch (Exception $exc) {
            $valval = $exc->getMessage();
            return FALSE;
        }
    }
    
    
    public function AddRemoveCommandsToGroups($commands, $groups) {
        foreach ($groups as $group) {
            
            /* @var $com Command */
            $com = $this->GetAutoRemoveCommandForGroup($group);
            if ($com != NULL && $com != FALSE) {
                $one = explode(",",$com->Params[0]["commandIds"]);
                $two = array_filter($one);
                $commands = array_unique(array_merge($two, $commands));
                $this->DeleteCommandById($com->CommandId);
            }
            $commands = Utils::FixArray($commands);
            $this->AddARemoveCommandToGroups(array($group), $commands);
            return true;
        }
    }

    public function AddARemoveCommandToGroups($groups, $commands) {
        $arr = array();
        foreach ($commands as $com) {
             $arr["commandIds"] .= $com . ",";
        }
        $arr["commandIds"] = rtrim($arr["commandIds"], ",");
        $com = Command::WithAllParameters($this->GenerateCommandId() . "", "RemoveCommand", "Main", "-1", "-1", $arr, "600", $groups, "600", Parameters::AUTO_GENERATED_REMOVE_COMMAND_NAME, "auto_generated");
        $this->AddCommand($com);
    }

    public function RemoveGroupFromCommand($command, $group) {
        try {
            $response = self::$dynamodb->updateItem([
                'TableName' => self::COMMANDS_TABLE,
                'Key' => [
                    'CommandId' => [ 'S' => $command]
                ],
                'ExpressionAttributeNames' => [
                    '#grp' => 'Groups'
                ],
                'ExpressionAttributeValues' => [
                    ':val1' => ['SS' => $group]
                ],
                'UpdateExpression' => 'DELETE #grp :val1'
            ]);

            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] == 200) {
                return true;
            } else {
                return $var["@metadata"]["statusCode"];
            }
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }

    function RemoveGroupsForDevices($devices, $groups) {
        foreach ($devices as $device) {
            $FunkeyId;
            $which = 0;
            if ($this->DeviceExists($device)) {
                $FunkeyId = $device;
                $which = 1;
            } else if ($this->DeviceExists($device, self::IMEI_INDEX)) {
                $FunkeyId = $this->GetFunkeyIdBySecondaryIndex($device, self::IMEI_INDEX);
                $which = 2;
            } else if ($this->DeviceExists($device, self::EXTRA_IMEI_INDEX)) {
                $FunkeyId = $this->GetFunkeyIdBySecondaryIndex($device, self::EXTRA_IMEI_INDEX);
                $which = 3;
            }
            if ($which == 0) {
                return "Failed reason: one or more of the devices could not be found (more " . $device . ")";
            }
            $dev = $this->GetDeviceByFunkeyId($FunkeyId);
            $groupsMerge = array_intersect($dev->Groups, $groups);
            $groupsMerge = Utils::FixArray($groupsMerge);
            $response = $this->RemoveGroupsForDevice($FunkeyId, $groupsMerge);

            if (!$response) {
                return "Failed to update group to one or more devices";
            }
        }
        return true;
    }

    function RemoveGroupsForDevice($device, $groups) {
        try {
            $response = self::$dynamodb->updateItem([
                'TableName' => self::DEVICES_TABLE,
                'Key' => [
                    'FunkeyId' => [ 'S' => $device]
                ],
                'ExpressionAttributeNames' => [
                    '#grp' => 'Groups'
                ],
                'ExpressionAttributeValues' => [
                    ':val1' => ['SS' => $groups]
                ],
                'UpdateExpression' => 'DELETE #grp :val1'
            ]);

            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] == 200) {
                return true;
            } else {
                return $var["@metadata"]["statusCode"];
            }
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }

    function AddGroupsToDevices($devices, $groups) {
        foreach ($devices as $device) {
            $count = strlen($device);
            $FunkeyId;
            $which = 0;
            if ($this->DeviceExists($device)) {
                $FunkeyId = $device;
                $which = 1;
            } else if ($this->DeviceExists($device, self::IMEI_INDEX)) {
                $FunkeyId = $this->GetFunkeyIdBySecondaryIndex($device, self::IMEI_INDEX);
                $which = 2;
            } else if ($this->DeviceExists($device, self::EXTRA_IMEI_INDEX)) {
                $FunkeyId = $this->GetFunkeyIdBySecondaryIndex($device, self::EXTRA_IMEI_INDEX);
                $which = 3;
            }
            if ($which == 0) {
                return "Failed reason: one or more of the devices could not be found (more " . $device . " count " . $count . " lastchar" . substr($device, -1) . ")";
            }
            $response = $this->AddGroupsToDevice($FunkeyId, $groups);

            if (!$response) {
                return "Failed to update group to one or more devices";
            }
        }
        return true;
    }

    function AddGroupsToDevice($device, $groups) {
        try {
            $response = self::$dynamodb->updateItem([
                'TableName' => self::DEVICES_TABLE,
                'Key' => [
                    'FunkeyId' => [ 'S' => $device]
                ],
                'ExpressionAttributeNames' => [
                    '#grp' => 'Groups'
                ],
                'ExpressionAttributeValues' => [
                    ':val1' => ['SS' => $groups]
                ],
                'UpdateExpression' => 'ADD #grp :val1'
            ]);

            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] == 200) {
                return true;
            } else {
                return $var["@metadata"]["statusCode"];
            }
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }

    function GenerateCommandId() {
        return $milliseconds = round(microtime(true) * 1000);
    }

    function AddCommand(Command $com) {
        try {
            /* @var $response Aws\Result */
            $response = self::$dynamodb->putItem([
                'TableName' => self::COMMANDS_TABLE,
                'Item' => $com->ToItem()
            ]);

            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] == 200) {
                return true;
            } else {
                return $var["@metadata"]["statusCode"];
            }
        } catch (Exception $exc) {
            $valval = $exc->getMessage();
            return $valval;
        }
    }

    function GetUsersByGroups($groups) {
        $return = array();

        try {
            $filterString = "";
            $values = array();
            for ($index = 0; $index < count($groups); $index++) {
                $values[":val" . $index] = ['S' => $groups[$index]];
                $filterString .= "contains (Groups, :val" . $index . ")";
                if ($index + 1 < count($groups)) {
                    $filterString .= " OR ";
                }
            }

            $params = [
                'TableName' => self::DEVICES_TABLE,
                'ExpressionAttributeValues' => $values,
                'FilterExpression' => $filterString
            ];

            /* @var $response Aws\Result  */
            $response = self::$dynamodb->scan($params);

            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] == 200) {
                $return["success"] = true;
            } else {
                $return["success"] = $var["@metadata"]["statusCode"];
            }

            $result = array();
            $items = $response->get('Items');
            foreach ($items as $item) {
                $dev = Device::FromItem($item);
                array_push($result, $dev);
            }
            $return["result"] = $result;

            return $return;
        } catch (Exception $exc) {
            $return["success"] = $exc->getTraceAsString();
            return $return;
        }
    }

    public function GetDeviceByFunkeyId($Id) {
        try {
            $params = [
                'TableName' => self::DEVICES_TABLE,
                'KeyConditionExpression' => 'FunkeyId = :v_id',
                'ExpressionAttributeValues' => [
                    ':v_id' => ['S' => $Id]
                ]
            ];
            $response = self::$dynamodb->query($params);
            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] != 200) {
                return $var["@metadata"]["statusCode"];
            }
            $items = $response["Items"];
            if (count($items == 1)) {
                return Device::FromItem($items[0]);
            }
            return "no item found";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function GetCommandById($id) {
        try {
            $params = [
                'TableName' => self::COMMANDS_TABLE,
                'KeyConditionExpression' => 'CommandId = :v_id',
                'ExpressionAttributeValues' => [
                    ':v_id' => ['S' => $id]
                ]
            ];
            $response = self::$dynamodb->query($params);
            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] != 200) {
                return $var["@metadata"]["statusCode"];
            }
            $items = $response["Items"];
            if (count($items) == 1) {
                return Command::FromItem($items[0]);
            }
            return FALSE;
        } catch (Exception $exc) {
           // echo $exc->getTraceAsString();
            return FALSE;
        }
    }

    public function ScanTest() {
        try {
            $params = [
                'TableName' => "commands",
                'ExpressionAttributeValues' => array(":val" => array('S' => "FileValue")),
                'ExpressionAttributeNames' => [
                    '#map' => 'Params',
                    '#key' => 'File'
                ],
                'FilterExpression' => "#map.#key = :val"
            ];
            $response = self::$dynamodb->scan($params);
            $items = $response["Items"];
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function GetFunkeyIdBySecondaryIndex($device, $index) {
        $primKey;
        if ($index == self::IMEI_INDEX) {
            $primKey = 'IMEI';
        } else if ($index == self::EXTRA_IMEI_INDEX) {
            $primKey = "ExtraIMEI";
        }
        $response = self::$dynamodb->query([
            'TableName' => self::DEVICES_TABLE,
            'IndexName' => $index,
            'KeyConditionExpression' => '#id = :v_dt',
            'ExpressionAttributeNames' => ['#id' => $primKey],
            'ExpressionAttributeValues' => [
                ':v_dt' => ['S' => $device],
            ],
            'ScanIndexForward' => true,
        ]);

        $items = $response["Items"];
        if (count($items) == 1) {
            return $items[0]["FunkeyId"]['S'];
        } else {
            return null;
        }
    }

    /* @var $device Device */

    public function setDeviceCommands($device, $commands) {
        try {
            $response = self::$dynamodb->updateItem([
                'TableName' => self::DEVICES_TABLE,
                'Key' => [
                    'FunkeyId' => [ 'S' => $device->FunkeyId]
                ],
                'ExpressionAttributeNames' => [
                    '#grp' => 'Commands'
                ],
                'ExpressionAttributeValues' => [
                    ':val1' => ['L' => Utils::CommandsListToStringList($commands)]
                ],
                'UpdateExpression' => 'set #grp = :val1'
            ]);

            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] == 200) {
                return true;
            } else {
                return $var["@metadata"]["statusCode"];
            }
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }

    public function SendPushNotificationToDevices($devices) {
        foreach ($devices as $device) {
            $this->SendPushNotificationToDevice($device);
        }
        return true;
    }

    public function SendPushNotificationToDevice($device) {
        try {
            $url = Parameters::PUSH_NOTIF_URL . "/" . $device;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function SetPostponeValueForGroup($groups, $postpone) {
        $result = $this->GetUsersByGroups($groups);
        $devices = array();
        /* @var $device Device */
        foreach ($result["result"] as $device) {
            array_push($devices, $device->FunkeyId);
        }
        array_unique($devices);

        foreach ($devices as $device) {
            $count = strlen($device);
            $FunkeyId;
            $which = 0;
            if ($this->DeviceExists($device)) {
                $FunkeyId = $device;
                $which = 1;
            } else if ($this->DeviceExists($device, self::IMEI_INDEX)) {
                $FunkeyId = $this->GetFunkeyIdBySecondaryIndex($device, self::IMEI_INDEX);
                $which = 2;
            } else if ($this->DeviceExists($device, self::EXTRA_IMEI_INDEX)) {
                $FunkeyId = $this->GetFunkeyIdBySecondaryIndex($device, self::EXTRA_IMEI_INDEX);
                $which = 3;
            }
            if ($which == 0) {
                return "Failed reason: one or more of the devices could not be found (more: " . $device . " countOfLetters " . $count . ")";
            }
            $response = $this->SetPostponeValueForDevice($FunkeyId, $postpone);

            if ($response != TRUE) {
                return "Failed to update postpone value to one or more devices reason?" . $response;
            }
        }
        return true;
    }
    
    public function SetPostponeValueForDevice($device, $postpone) {
        try {
            $response = self::$dynamodb->updateItem([
                'TableName' => self::DEVICES_TABLE,
                'Key' => [
                    'FunkeyId' => [ 'S' => $device]
                ],
                'ExpressionAttributeNames' => [
                    '#grp' => 'PostponeValue'
                ],
                'ExpressionAttributeValues' => [
                    ':val1' => ['N' => $postpone]
                ],
                'UpdateExpression' => 'set #grp = :val1'
            ]);

            $var = $response->toArray();
            if ($var["@metadata"]["statusCode"] == 200) {
                return TRUE;
            } else {
                return " statuscode: " . $var["@metadata"]["statusCode"];
            }
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }

}

//echo 'done';
