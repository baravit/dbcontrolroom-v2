<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Device {
    public $FunkeyId;
    public $IMEI;
    public $ExtraIMEI;
    public $RegId;
    public $Groups;
    public $Commands;
    
    public function __construct() {

    }
    
    public static function FromItem($item){
        $instance = new self();
        $instance->FunkeyId  = $item['FunkeyId']['S'];
        if(array_key_exists('S', $item['IMEI'])) {
            $instance->IMEI = $item['IMEI']['S'];
        } else {
            $instance->IMEI = Parameters::NO_IMEI;            
        }
        if(array_key_exists('S', $item['ExtraIMEI'])) {
            $instance->ExtraIMEI = $item['ExtraIMEI']['S'];
        } else {
            $instance->ExtraIMEI = Parameters::NO_EXTRA_IMEI;            
        }
        if(array_key_exists('S', $item['RegId'])) {
            $instance->RegId = $item['RegId']['S'];
        } else {
            $instance->RegId = Parameters::NO_REGID;            
        }
        
        $instance->Groups = $item['Groups']['SS'];
        $temp = $item['Commands']['L'];
        $arr = array();
        foreach ($temp as $cmdStr) {
            array_push($arr, Command::FromString($cmdStr['S']));
        }
        $instance->Commands = $arr;
        
        return $instance;
    }
   
    
    public function __toString() {
        return $this->FunkeyId;
    }
}
//echo 'done';