<?php

class Parameters {
    const MAX_DEVICES_TO_DISPLAY = 300;
    const NO_REGID = "NO_REGID";
    const NO_EXTRA_IMEI = "NO_EXTRA_IMEI";
    const NO_IMEI = "NO_IMEI";
    const FUNKEYDATASERVER_URL = "http://funkeydataserver.ap-south-1.elasticbeanstalk.com";
    //const UPDATE_TABLE_URL = 'http://192.168.0.127:8090/data/Update';
    const UPDATE_TABLE_URL = 'http://funkeydataserver.ap-south-1.elasticbeanstalk.com/data/Update';
    const PUSH_NOTIF_URL = 'http://funkeydataserver.ap-south-1.elasticbeanstalk.com/push/sendPushMessageToDevice';
    const AUTO_GENERATED_REMOVE_COMMAND_NAME = "auto_generated_remove_command";
}
