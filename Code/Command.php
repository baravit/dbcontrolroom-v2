<?php

class Command {

    public $CommandId;
    public $CommandName;
    public $Groups = array();
    public $Type;
    public $Mode;
    public $NextCommandId;
    public $Params = array();
    public $Comments;    
    public $Repeat;
    public $Timeout;
    public $FailurePostpone;
    public function __construct() {
        
    }
    
    public static function WithAllParameters($CommandId2, $Type2, $Mode2, $Repeat2, $NextCommandId2, $Params2, $Timeout2, $Groups2, $failurePostpone2, $commandName2, $comments2) {
        $instance = new self();
        $instance->CommandId = $CommandId2;
        $instance->Type = $Type2;
        $instance->Mode = $Mode2;
        $instance->Repeat = $Repeat2;
        $instance->NextCommandId = $NextCommandId2;
        $instance->Params = $Params2;
        $instance->Timeout = $Timeout2;
        $instance->Groups = $Groups2;
        $instance->FailurePostpone = $failurePostpone2;
        if(Utils::IsEmptyString($comments2)){
            $comments2 = "no_comments";
        }
        $instance->Comments = $comments2; 
        if(Utils::IsEmptyString($commandName2)) {
            $commandName2 = "no_command_name";
        }
        $instance->CommandName = $commandName2;
        return $instance;
    }
    
    
    public function ToItem() {
        $item = array();
        $item["CommandId"] = ['S' => $this->CommandId];
        $item["Type"] = ['S' => $this->Type];
        $item["Mode"] = ['S' => $this->Mode];
        $item["Repeat"] = ['N' => $this->Repeat];
        $item["NextCommandId"] = ['S' => $this->NextCommandId];
        $item["Timeout"] = ['N' => $this->Timeout];
        if(count($this->Groups) > 0) {
            $item["Groups"] = ['SS' => $this->Groups];
        }
        $item["FailurePostpone"] = ['N' => $this->FailurePostpone];
        if (count($this->Params) > 0) {
            $ParamsTemp = array();
            foreach ($this->Params as $key => $value) {
                $ParamsTemp[$key] = ['S' => $value];
            }
            $item["Params"] = ['M' => $ParamsTemp];
        }
        if(!Utils::IsEmptyString($this->Comments)) {
            $item["Comments"] = ['S' => $this->Comments];
        }
        if(!Utils::IsEmptyString($this->CommandName)) {
            $item["CommandName"] = ['S' => $this->CommandName];
        }
        return $item;
    }
    
    public static function FromString($str) {
        $instance = new self();
        $json = json_decode($str);
        $instance->CommandId = $json->CommandId;
        $instance->Type = $json->Type;
        $instance->Mode = $json->Mode;
        $instance->Repeat = $json->Repeat;
        $instance->NextCommandId = $json->NextCommandId;
        $instance->Timeout = $json->Timeout;
        $instance->Groups = $json->Groups;
        $instance->FailurePostpone = $json->FailurePostpone;
        if(property_exists($json, "Comments")) {
            $instance->Comments = $json->Comments;
        }
        if(property_exists($json, "CommandName")) { 
            $instance->CommandName = $json->CommandName;
        }
        $paramsClass = $json->Params;
        $params = array();
        foreach ($paramsClass as $key => $value) {
            $params[$key] = $value;
        }
        $instance->Params = $params;

        return $instance;
    }
    
    public static function FromItem($item) {
        $instance = new self();
        $instance->CommandId = $item["CommandId"]['S'];
        $instance->Type = $item["Type"]['S'];
        $instance->Mode = $item["Mode"]['S'];
        $instance->Repeat = $item["Repeat"]['N'];
        $instance->NextCommandId = $item["NextCommandId"]['S'];
        $instance->Timeout = $item["Timeout"]['N'];
        $instance->Groups = $item["Groups"]['SS'];
        $instance->FailurePostpone = $item["FailurePostpone"]['N'];
        if(array_key_exists("Comments", $item)) {
            $instance->Comments = $item["Comments"]['S'];
        }
        if(array_key_exists("CommandName", $item)) {
            $instance->CommandName = $item["CommandName"]['S'];
        }
        
        
        $temp = $item["Params"]['M'];
        $map = array();
        foreach ($temp as $key => $value) {
            array_push($map, array($key => $value['S']));
        }
        $instance->Params = $map;
        
        return $instance;
    }
    
    public function __toString() {
        return $this->CommandId;
    }
    
    public function ToJSONString() {
        $jsonObj = array();
        $jsonObj["CommandId"] = $this->CommandId;
        $jsonObj["Type"] = $this->Type;
        $jsonObj["Mode"] = $this->Mode;
        $jsonObj["Repeat"] = $this->Repeat;
        $jsonObj["NextCommandId"] = $this->NextCommandId;
        $jsonObj["Timeout"] = $this->Timeout;
        $jsonObj["Groups"] = $this->Groups;
        $jsonObj["FailurePostpone"] = $this->FailurePostpone;
        $jsonObj["Params"] = $this->Params;
        $jsonObj["Comments"] = $this->Comments;
        $jsonObj["CommandName"] = $this->CommandName;
        
        $string = json_encode($jsonObj, JSON_UNESCAPED_SLASHES);
        return $string;
    }
}
?>


