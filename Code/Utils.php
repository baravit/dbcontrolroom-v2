<?php

class Utils {

    public static $IsEvenRow = true;

    public static function DataToTableFormat($params) {
        $ret = "<tr class=";
        if (Utils::$IsEvenRow) {
            $ret .= "\"even gradeX\"";
        } else {
            $ret .= "\"odd gradeX\"";
        }
        $ret .= ">";
        foreach ($params as $p) {
            $ret .= "<td class=\"center lol\">" . $p . "</td>";
        }
        $ret .= "</tr>";
        Utils::$IsEvenRow = !Utils::$IsEvenRow;
        return $ret;
    }
    
    
    public static function ListToString($list, $UseSpaces = true) {
        $str = "";
        foreach ($list as $value) {
            if($UseSpaces) {
                $str .= $value . ', '; 
            } else {
                $str .= $value . ',';                 
            }
        }
        return $str;
    }
    
    public static function ObjectListToArray($list) {
        $array = array();
        foreach ($list as $object) {
            $map = array();
            foreach ($object as $key => $value) {
                $map[$key] = $value;
            }
            array_push($array, $map);
        }
        return $array;
    }
    
    public static function ArrayToJSONStrig($array) {
        return json_encode($array, JSON_UNESCAPED_SLASHES);
    }
    
    public static function MapToString($map) {
        return json_encode($map, JSON_UNESCAPED_SLASHES);
    }
    
    public static function DataToTableFormatAuto($object) {
        $ret = "<tr class=";
        if (Utils::$IsEvenRow) {
            $ret .= "\"even gradeX\"";
        } else {
            $ret .= "\"odd gradeX\"";
        }
        $ret .= ">";
        foreach ($object as $key => $value) {
            if(!is_array($value)) {
                $ret .= "<td class=\"center lol\">" . $value . "</td>";
            } else {
                $ret .= "<td class=\"center lol\">" . Utils::ArrayToJSONStrig($value) . "</td>";
            }
        }
        $ret .= "</tr>";
        Utils::$IsEvenRow = !Utils::$IsEvenRow;
        return $ret;
    }
    public static function DataToTableHeader($object, $sepecial = FALSE) {
        $ret = "<tr>";
        
        foreach ($object as $key => $value) {
            if(!$sepecial) {
                $ret .= "<th>" . $key . "</th>";
            } else {
                $ret .= "<th>" . $key . "__</th>";
            }
        }
        $ret .= "</tr>";
        return $ret;
    }
    
    public static function BoolToString($bool) {
        if($bool) {
            return "true";
        }
        return "false";
        
    }
    
    public static function CommandsListToStringList($commands) {
        $stringList = array();
        /* @var $command Command */
        foreach ($command as $commands) {
            array_push($stringList, $command->ToJSONString());
        }
        return $stringList;
    }
    
    public static function IsEmptyString($string) {
        if($string == ""){
            return TRUE;
        } else if(count_chars($string) == 0) {
            return TRUE;
        }
        return FALSE;
    }
    
    public static function DataToFileFormat($data) {
        if(!is_array($data) || count($data) == 0 || !is_object($data[0])){
            return false;
        }
        $filedata = "";
        foreach ($data[0] as $key => $value) {
            $filedata .= $key . "\t";
        }
        $filedata .= "\n";
        foreach ($data as $object) {
            foreach ($object as $key => $value) {
                if(!is_array($value)) {
                    $filedata .= $value . "\t";
                } else {
                     $filedata .= Utils::ArrayToJSONStrig($value) . "\t";
                }
            }
            $filedata .= "\n";
        }
        
        return $filedata;
    }

    public static function FixArray($array) {
        $arr = array();
        end($array);
        for ($i = 0; $i <= key($array); $i++) {
            if (isset($array[$i])) {
                array_push($arr, $array[$i]);
            }
        }
        return $arr;
    }
    
    
    public static function IsTrue($bool) {
        if((is_bool($bool) && $bool) || $bool == "true") {
            return TRUE;
        }
        return FALSE;   
    }
    
    public static function HandleTypeParms($params, $type) {
        switch ($type) {
            case "UpdateMiner":
                $str = "{";
                $shouldAddComma = FALSE;
                if($params[0] != null && count($params[0]) > 0){
                    $str.= "\"interval\":".$params[0];
                    $shouldAddComma = TRUE;
                }
                if($params[1] != null && count($params[1]) > 0){
                    if($shouldAddComma) {
                        $str .= ", ";
                    }
                    $str.= "\"enabled\":".$params[1];
                }
                $str .= "}";
                return $str;

            default:
                return $params;
        }
    }
}

?>


