﻿

<%@ Page Language="C#" MasterPageFile="~/pages/MasterPage.master" Inherits="DBControlRoom.AddGroup"  EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
   <div class="row">
      <div class="col-lg-12">
         <h1 class="page-header">Forms</h1>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <div class="row">
      <div class="col-lg-12">
         <div style="margin-left: 20px; margin-right: 20px;" class="panel panel-default">
            <div class="panel-heading">
               Add groups to devices
            </div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-lg-12">
                     <form role="form">
                        <div class="form-group">
                           <label>Choose Devices</label>
                           <input name="fileUpload" id="fileUpload" type="file">
                           <p class="help-block">Upload file or...</p>
                           <textarea name="devices" id="devices" class="form-control" rows="3" placeholder="eg: 123,124,125..."></textarea>
                        </div>
                        <div class="form-group">
                           <label>Choose Groups</label>
                           <textarea name="groups" id="groups" class="form-control" rows="3" placeholder="eg: g1,g12..."></textarea>
                        </div>
                         <div class="panel panel-default">
			               <div class="panel-heading">
			                  Groups in DB
			               </div>
			               <!-- /.panel-heading -->
			               <div class="panel-body">
			                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
			                     <thead>
			                        <tr>
			                           <th>Groups</th>
			                        </tr>
			                     </thead>
			                     <tbody>
			                        <tr class="odd gradeX">
			                           <td class="center">X</td>
			                        </tr>
			                        <tr class="even gradeX">
			                           <td class="center">X2</td>
			                        </tr>
			                     </tbody>
			                  </table>
			               </div>
			               <!-- /.panel-body -->
			            </div>
			            <!-- /.panel panel-body -->
			            <asp:Button id="button1" runat="server" Text="Execute" CssClass="btn btn-default" OnClick="button1Clicked" />
                        <!--<button type="submit" class="btn btn-default">Submit Button</button>-->
                        <button type="reset" class="btn btn-default">Reset Button</button>
                     </form>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
               <!-- /.row -->
            </div>
            <!-- /.panel-body -->
           
         </div>
         <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderScripts" Runat="Server">
		    <script>
    	$(document).ready(function() {
        	$('#dataTables-example').DataTable({
            	responsive: true
        	});

        	$('#dataTables-example tbody').on( 'click', 'tr', function () {
	        	$(this).toggleClass('selected');
	        	var info = $(this).first().text(); // getting the selected row info
	        	info = info.replace(/\s+/g, ''); // removing white spaces
	        	info = info.replace(/(\r\n|\n|\r)/gm,""); // removing line breakers
	        	var add = true;
	        	var finalContent = '';
	        	var allgroups = $("#groups").val().split(",");
	        	for (var i = 0; i < allgroups.length; i++) {
	        		if(allgroups[i] != info) {
	        			if (allgroups[i] != "")
	        				finalContent += allgroups[i]+",";
	        		}
	        		else
	        			add = false;
	        	}	
	        	if (add)
	        		finalContent += info+",";

	        	$("#groups").val(finalContent);
    	});

    });
    </script>
</asp:Content>

