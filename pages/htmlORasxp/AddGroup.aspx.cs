﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;
namespace DBControlRoom
{
	
	public partial class AddGroup : System.Web.UI.Page
	{
		public string tableData = "";
		public string tableHead = "";
		protected void Page_Load(object sender, EventArgs e)
		{
			//TODO get groups from the server
			tableHead = "<tr>\n            <th>Rendering engine</th>\n            <th>Browser</th>\n            <th>Platform(s)</th>\n            <th>Engine version</th>\n            <th>CSS grade</th>\n        </tr>";
			tableData = "<tr class=\"odd gradeX\">\n            <td>Trident</td>\n            <td>Internet Explorer 4.0</td>\n            <td>Win 95+</td>\n            <td class=\"center\">4</td>\n            <td class=\"center\">X</td>\n        </tr>";
		}
		/*HEAD
		<tr>
            <th>Rendering engine</th>
            <th>Browser</th>
            <th>Platform(s)</th>
            <th>Engine version</th>
            <th>CSS grade</th>
        </tr>

		BODY
		<tr class="odd gradeX">
            <td>Trident</td>
            <td>Internet Explorer 4.0</td>
            <td>Win 95+</td>
            <td class="center">4</td>
            <td class="center">X</td>
        </tr>
		*/

		public void button1Clicked (object sender, EventArgs args)
		{
			string devices = Request.Form ["devices"];
			string groups = Request.Form ["groups"];
			List<string> deviceList = new List<string>();
			List<string> groupList = new List<string>();
			HttpPostedFile filePosted = Request.Files["fileUpload"];
			if ((devices.Length == 0 &&(filePosted == null || filePosted.ContentLength ==0)) || groups.Length == 0) {
				throw new Exception ("Missing parameters");
			}
			if (devices.Length > 0) {
				deviceList = new List<string>(devices.Split (','));
			}

			groupList = new List<string>(groups.Split (','));

			if (filePosted != null && filePosted.ContentLength > 0) {
				//TODO filePosted.SaveAs(path); maybe...
				StreamReader sr = new StreamReader(filePosted.InputStream);
				string devicesFromFile = sr.ReadToEnd ();
				deviceList.AddRange (devicesFromFile.Split (','));
			}
			for (int i = 0; i < deviceList.Count; i++) {
				if (deviceList[i] == "") {
					deviceList.RemoveAt (i);
					i--;
				}
			}
			for (int i = 0; i < groupList.Count; i++) {
				if (groupList[i] == "") {
					groupList.RemoveAt (i);
					i--;
				}
			}


			DataAccessLayer dal = new DataAccessLayer ();
			dal.AddGroupsToDevices (deviceList, groupList);
		}
	}
}

