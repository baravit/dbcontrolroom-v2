<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php include 'MPHead.php'; ?>
    </head>
    <body>
        <style>
            td > input {
                width: 210px;

            }
        </style>
        <?php
        include '../Code/DAL.php';
        if (isset($_POST['subBtn'])) {
            $CommandId = str_replace(' ', '', $_POST['commandId']);
            if ($CommandId == null) {
                $CommandId = str_replace(' ', '', $_POST['commandIdH']);
            }
            $modeText = $_POST['modeText'];
            $comments = $_POST['comments'];
            $commandName = $_POST['commandName'];
            $typeText = $_POST['typeText'];
            $timeout = (string) (intval($_POST['timeout']));
            if ($timeout == 0) {
                $timeout = 120 * 1000;
            }
            if (array_key_exists("repeat", $_POST) && !empty($_POST['repeat']) && $_POST['repeat'] != "-1") {
                $repeat = (string) (intval($_POST['repeat']));
            } else {
                $repeat = "-1";
            }
            $failurePostpone = (string) (intval($_POST['failurePostpone']));
            if ($failurePostpone == 0) {
                $failurePostpone = 24 * 60 * 60 * 1000;
            }
            if (array_key_exists("nextCommandId", $_POST) && !empty($_POST['nextCommandId'])) {
                $nextCommandId = $_POST['nextCommandId'];
            } else {
                $nextCommandId = "-1";
            }

            $groupStr = str_replace(" ", "", $_POST['groups']);
            $groupsArray = explode(",", $groupStr);
            $groupsArray = array_diff($groupsArray, [" "]);
            $groupsArray = array_filter($groupsArray);

            $params = array();
            for ($index = 0; $index < 50; $index++) {
                $key = $_POST["paramsN" . $index];
                $value = $_POST["paramsV" . $index];
                if ($key != null && $value != null & count($key) > 0 && count($value) > 0) {
                    $params[$key] = $value;
                }
            }

            $com = Command::WithAllParameters($CommandId, $typeText, $modeText, $repeat, $nextCommandId, $params, $timeout, $groupsArray, $failurePostpone, $commandName, $comments);

            $dal = new DAL();
            $success = $dal->AddCommand($com);

            if (Utils::IsTrue($success)) {
                header("Location: Success.php");
            } else {
                session_start();
                $_SESSION["Error"] = $success;
                header("Location: Error.php");
            }
        }
        ?>

        <?php include 'MPBody1.php'; ?>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Command</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div style="margin-left: 20px; margin-right: 20px;" class="panel panel-default">
                    <div class="panel-heading">
                        Edit Command
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form name="form2" id="form2" role="form" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Enter command Id</label>
                                        <input id="commandLoad" name="commandLoad" class="form-control" placeholder="ex: Steven" >
                                        <button type="Button" id="press">Load</button><br><br>
                                    </div>
                                    <div id="formdiv" name="formdiv">
                                    </div>
                                </form>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <?php include 'MPBody2.php'; ?>

        <script>
            $(document).ready(function () {
                abc();
            });
            function abc() {
                var counter = 0;
                var t = $('#dataTables-example2').DataTable({
                    searching: false,
                    paging: false,
                    ordering: false
                });

                var IsOnInput = false;
                $('#dataTables-example2 tbody').on('click', 'tr', function () {
                    if (!IsOnInput)
                        $(this).toggleClass('selected');
                    IsOnInput = false;
                });
                $('#dataTables-example2 tbody').on('click', 'input', function () {
                    IsOnInput = true;
                });


                $('#addRow').on('click', function () {
                    t.row.add([
                        '<input name=\"paramsN' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">',
                        '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                    ]).draw(false);

                    counter++;
                });

                $('#remRow').on('click', function () {
                    var rows = t
                            .rows('.selected')
                            .remove()
                            .draw();
                });

                $('#dataTables-example').DataTable({
                    responsive: true,
                    "lengthMenu": [10, 25, 50, 75, 100]
                });

                $('#dataTables-example tbody').on('click', 'tr', function () {
                    $(this).toggleClass('selected');
                    var info = $(this).first().text(); // getting the selected row info
                    info = info.replace(/\s+/g, ''); // removing white spaces
                    info = info.replace(/(\r\n|\n|\r)/gm, ""); // removing line breakers
                    var add = true;
                    var finalContent = '';
                    var allgroups = $("#groups").val().split(",");
                    for (var i = 0; i < allgroups.length; i++) {
                        if (allgroups[i] != info) {
                            if (allgroups[i] != "")
                                finalContent += allgroups[i] + ",";
                        } else
                            add = false;
                    }
                    if (add)
                        finalContent += info + ",";

                    $("#groups").val(finalContent);
                });

                $('#mode').on('change', function () {
                    $('#modeText').val($(this).val());
                });
                $('#type').on('change', function () {
                    $('#typeText').val($(this).val());
                    var type = $(this).val();
                    if (type == 'Download') {
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"url\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"Use3G\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"(optional) Default: false\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"useOldDownloader\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"(optional) Default: false\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                    } else if (type == "Install") {
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"File\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;

                    } else if (type == "Start") {
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"packageName\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"isService\" placeholder=\"(optional) Default: false\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"serviceAction\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"only required if isService is true\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"className\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"only required if isService is true\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                    } else if (type == "Stop") {
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"packageName\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                    } else if (type == "RunCommand") {
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"command\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"Expression\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"(optional) ex: (abc OR (dfg AND hij))\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"useSystemApp\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"timeout\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"(optional)(if using systemapp) time in milliseconds\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"NextCommandIdFailure\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"(optional)Next command to run in case of failure\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                    } else if (type == "RemoveCommand") {
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"commandIds\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"ex: 123,124,125\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                    } else if (type == "Delete") {
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"packageName\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                    } else if (type == "UpdatePartameter") {
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"key\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"value\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" value=\"type\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"ex: int, long, String, boolean\" onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                    } else if (type == "UpdateMiner") {
                        t.row.add([
                            '<input name=\"paramsN' + counter + '\" class=\"from-control\" placeholder=\"Miner_Name\" onkeyup=\"Expand(this);\">',
                            '<input name=\"paramsV' + counter + '\" class=\"from-control\" placeholder=\"ex: { &quot;enabled&quot; : false } \"  onkeyup=\"Expand(this);\">'
                        ]).draw(false);
                        counter++;
                    }
                });

                $('#override').on('click', function () {
                    $('#commandId').prop('disabled', function (i, v) {
                        return !v;
                    });
                });


                function Expand(textbox) {
                    if (!textbox.startW) {
                        textbox.startW = textbox.offsetWidth;
                    }

                    var style = textbox.style;

                    //Force complete recalculation of width
                    //in case characters are deleted and not added:
                    style.width = 0;

                    //http://stackoverflow.com/a/9312727/1869660
                    var desiredW = textbox.scrollWidth;
                    //Optional padding to reduce "jerkyness" when typing:
                    desiredW += textbox.offsetHeight;
                    var wid = Math.max(desiredW, textbox.startW);
                    var parentwid = textbox.parentElement.clientWidth;

                    style.width = Math.min(wid, parentwid) + 'px';

                }

                $("#press").click(function () {
                    $.ajax({
                        url: "EditCommand_1.php?id=" + $("#commandLoad").val(),
                        context: document.body,
                        success: function (data1) {
                            var generatedHtml = data1; // build your html from the data object
                            $('#formdiv').empty();
                            $('#formdiv').append(generatedHtml);
//                            $('#dataTables-example').DataTable({
//                                responsive: true,
//                                "lengthMenu": [10, 25, 50, 75, 100]
//                            });
//                            t = $('#dataTables-example2').DataTable({
//                                searching: false,
//                                paging: false,
//                                ordering: false
//                            });
                            abc();
                        }
                    });
                });

                $("#press2").click(function () {
//                    $('#dataTables-example').destory();
//                    $('#dataTables-example').DataTable({
//                        responsive: true,
//                        "lengthMenu": [10, 25, 50, 75, 100]
//                    });
//                    t.destroy();
//                    t = $('#dataTables-example2').DataTable({
//                        searching: false,
//                        paging: false,
//                        ordering: false
//                    });
                    abc();
                });


                $("#copyButton").click(function () {
                    $('#commandId').prop('disabled', function (i, v) {
                        return !v;
                    });
                    copyToClipboard(document.getElementById("commandId"));
                    $('#commandId').prop('disabled', function (i, v) {
                        return !v;
                    });
                });

                function copyToClipboard(elem) {
                    // create hidden text element, if it doesn't already exist
                    var targetId = "_hiddenCopyText_";
                    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
                    var origSelectionStart, origSelectionEnd;
                    if (isInput) {
                        // can just use the original source element for the selection and copy
                        target = elem;
                        origSelectionStart = elem.selectionStart;
                        origSelectionEnd = elem.selectionEnd;
                    } else {
                        // must use a temporary form element for the selection and copy
                        target = document.getElementById(targetId);
                        if (!target) {
                            var target = document.createElement("textarea");
                            target.style.position = "absolute";
                            target.style.left = "-9999px";
                            target.style.top = "0";
                            target.id = targetId;
                            document.body.appendChild(target);
                        }
                        target.textContent = elem.textContent;
                    }
                    // select the content
                    var currentFocus = document.activeElement;
                    target.focus();
                    target.setSelectionRange(0, target.value.length);

                    // copy the selection
                    var succeed;
                    try {
                        succeed = document.execCommand("copy");
                    } catch (e) {
                        succeed = false;
                    }
                    // restore original focus
                    if (currentFocus && typeof currentFocus.focus === "function") {
                        currentFocus.focus();
                    }

                    if (isInput) {
                        // restore prior selection
                        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
                    } else {
                        // clear temporary content
                        target.textContent = "";
                    }
                    return succeed;
                }
            }
        </script>

    </body>
</body>
</html>
