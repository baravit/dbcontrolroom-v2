<?php
    include '../Code/DAL.php';
    $groupsArray2 = explode(",", $_POST['groupsH']);
    $groupsArray2 = array_diff($groupsArray2, [""]);
    $groupsArray2 = array_unique($groupsArray2);
    $dal = new DAL();
    $results2 = $dal->GetUsersByGroups($groupsArray2);
    $content = Utils::DataToFileFormat($results2["result"]);

    //$filename = "yourfile.txt";
    $filename = tempnam(sys_get_temp_dir(), 'data');
    $f = fopen($filename, 'w');
    fwrite($f, $content);
    fclose($f);
    //echo $content;
   // flush();
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Length: " . filesize("$filename") . ";");
    header("Content-Disposition: attachment; filename=YourFile.txt");
    header("Content-Type: application/octet-stream; ");
    header("Content-Transfer-Encoding: binary");
    ob_clean();
    readfile($filename);
