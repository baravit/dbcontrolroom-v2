<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php include 'MPHead.php'; ?>
    </head>
    <body>
        
        <?php
        include '../Code/DAL.php';
        if (isset($_POST['expBtn'])) {
            /* require_once '../Code/PHPExcel/PHPExcel.php';
            
            $objPHPExcel = new PHPExcel();

            // Set document properties
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                    ->setLastModifiedBy("Maarten Balliauw")
                    ->setTitle("PHPExcel Test Document")
                    ->setSubject("PHPExcel Test Document")
                    ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                    ->setKeywords("office PHPExcel php")
                    ->setCategory("Test result file");
            $objPHPExcel->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);

            $groupsArray2 = explode(",", $_POST['groupsH']);
            $groupsArray2 = array_diff($groupsArray2, [""]);
            $groupsArray2 = array_unique($groupsArray2);

            $dal = new DAL();
            $results2 = $dal->GetUsersByGroups($groupsArray2);

            $devices = $results2["result"];
            $data = array(array("FunkeyId","IMEI","ExtraIMEI","RegId", "Groups", "Commands"));
             @var $devi Device 
            foreach ($devices as $devi) {
                array_push($data, array("FunkeyId" => $devi->FunkeyId, "IMEI" => $devi->IMEI, "ExtraIMEI" => $devi->ExtraIMEI, "RegId" => $devi->RegId, "Groups" => Utils::ListToString($devi->Groups), "Commands" => Utils::ArrayToJSONStrig(Utils::ObjectListToArray($devi->Commands))));
            }
            // Add some data
            $objPHPExcel->getActiveSheet()->fromArray($data, NULL, 'A1');

            ob_end_clean();
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="your_name.xls"');
            header('Cache-Control: max-age=0');

            // Do your stuff here

            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // This line will force the file to download
            $writer->save('php://output');
            */
            
            $groupsArray2 = explode(",", $_POST['groupsH']);
            $groupsArray2 = array_diff($groupsArray2, [" "]);
            $groupsArray2 = array_unique($groupsArray2);
            $groupsArray2 = array_filter($groupsArray2); 
           
            $dal = new DAL();
            $results2 = $dal->GetUsersByGroups($groupsArray2);
            $content = Utils::DataToFileFormat($results2["result"]);
            
            //$filename = "yourfile.txt";
            $filename = tempnam(sys_get_temp_dir(), 'data');
            $f = fopen($filename, 'w');
            fwrite($f, $content);
            fclose($f);

            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Length: " . filesize("$filename") . ";");
            header("Content-Disposition: attachment; filename='".basename($tmp)."'");
            header("Content-Type: application/octet-stream; ");
            header("Content-Transfer-Encoding: binary");
            
            readfile($filename);
        }
        // put your code here
        if (isset($_POST['subBtn'])) {
            $groupsArray = explode(",", $_POST['groups']);
            $groupsArray = array_diff($groupsArray, [" "]);
            $groupsArray = array_unique($groupsArray);
            $groupsArray = array_filter($groupsArray); 

            $dal = new DAL();
            $results = $dal->GetUsersByGroups($groupsArray);

            $success = $results["success"];
            if(Utils::IsTrue($success)) {
                $result = $results["result"];
                if (count($result) > Parameters::MAX_DEVICES_TO_DISPLAY) {
                    $modelText = "Something something something <br> info LUL <br> # O' Devices: " . count($result);
                } else {
                    $modeText = "";
                }
            } else {
                $_SESSION["Error"] = $success;
                header("Location: Error.php");
            }
        }
        ?>
        <?php include 'MPBody1.php'; ?>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Get Users By Groups</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div style="margin-left: 20px; margin-right: 20px;" class="panel panel-default">
                    <div class="panel-heading">
                        Get Users By Groups
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form role="form">                      
                                    <div class="form-group">
                                        <label>Choose Groups</label>
                                        <textarea name="groups" id="groups" class="form-control" rows="3" placeholder="eg: g1,g12..."></textarea>
                                        
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Groups in DB
                                        </div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr><th>Group name</th></tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    try {
                                                        $dal = new DAL();
                                                        foreach ($dal->GetAllGroups() as $grp) {
                                                            $rows .= Utils::DataToTableFormat(array($grp));
                                                        }
                                                    } catch (Exception $exc) {
                                                        echo $exc->getTraceAsString();
                                                    }

                                                    echo $rows;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel panel-body -->
                                    <!--<asp:Button id="button1" runat="server" Text="Execute" CssClass="btn btn-default" OnClick="SubmitButton" />-->
                                    <button type="submit" name="subBtn" id="subBtn" class="btn btn-success">Submit Button</button>
                                    <button type="reset" class="btn btn-info">Reset Button</button>
                                    <input id="tb" name="tb" runat="server" type="hidden">
                                    <div class="form-group">
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Results
                                        </div>
                                        <!-- /.panel-heading -->

                                        <div class="panel-body">
                                            <button style="margin-bottom: 11px;" type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#myModal">Export</button>
                                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example2">
                                                <thead>
                                                    <tr><th>FunkeyId</th><th>IMEI</th><th>ExtraIMEI</th><th>HasRegid</th><th>Groups</th><th>Commands</th></tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (count($result) > 0 && count($modelText) == 0) {
                                                        $rows = "";
                                                        /* @var $device Device */
                                                        foreach ($result as $device) {
                                                            $commandsStr = "";
                                                            foreach ($device->Commands as $command) {
                                                                /* @var $command Command */
                                                                $commandsStr .= $command->CommandId . ',';
                                                            }
                                                            $cmds = "<input class=\"btn btn-primary\" type=\"button\" onclick=\"location.href='DisplayCommands.php?ids=" . $commandsStr . "';\" value=\"View " . count($device->Commands) . " Commands\" />";

                                                            $rows .= Utils::DataToTableFormat(array($device->FunkeyId, $device->IMEI,
                                                                $device->ExtraIMEI,
                                                                Utils::BoolToString($device->RegId == Parameters::NO_REGID),
                                                                Utils::ListToString($device->Groups),
                                                                $cmds));
                                                        }
                                                        echo $rows;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>

                                </form>

                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <form action="Export.php" method="post">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            <?php echo $modelText; ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name='expBtn' id='expBtn' class="btn btn-primary">Export</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
                <input type="hidden" name="ModelText" id="ModelText" value='<?php
                if (count($modelText) > 0) {
                    echo $modelText;
                }
                ?>'>
                <input type="hidden" name='groupsH' id='groupsH' value='<?php
                if (count($groupsArray) > 0) {
                    echo Utils::ListToString($groupsArray, false);
                }
                ?>'>
            </form>
        </div>
        <!-- /.modal -->
        

<?php include 'MPBody2.php'; ?>

        <script>
            $(document).ready(function () {
                if ($('#ModelText').val() != '') {
                    $("#myModal").modal();
                }
                
                $('#dataTables-example2').DataTable({
                    responsive: true,
                    "lengthMenu": [10, 25, 50, 75, 100]
                });
                
                $('#dataTables-example').DataTable({
                    responsive: true,
                    "lengthMenu": [10, 25, 50, 75, 100]
                });

                $('#dataTables-example tbody').on('click', 'tr', function () {
                    $(this).toggleClass('selected');
                    var info = $(this).first().text(); // getting the selected row info
                    info = info.replace(/\s+/g, ''); // removing white spaces
                    info = info.replace(/(\r\n|\n|\r)/gm, ""); // removing line breakers
                    var add = true;
                    var finalContent = '';
                    var allgroups = $("#groups").val().split(",");
                    for (var i = 0; i < allgroups.length; i++) {
                        if (allgroups[i] != info) {
                            if (allgroups[i] != "")
                                finalContent += allgroups[i] + ",";
                        } else
                            add = false;
                    }
                    if (add)
                        finalContent += info + ",";

                    $("#groups").val(finalContent);
                });

            });
        </script>
    </body>
</html>
