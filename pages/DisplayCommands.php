<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php include 'MPHead.php'; ?>
    </head>
    <body>

        <?php
        include '../Code/DAL.php';
        // put your code here
        $tableData = "";
        $tableHead = "";
        $dal = new DAL();
        
        $ids = $_GET["ids"];
        $ids = explode(",", $ids);
        $ids = array_diff($ids, [""]);
        $ids = array_unique($ids);
        
        $commands = array();
        foreach ($ids as $id) {
            array_push($commands, $dal->GetCommandById($id));
        }
        $sampleCommand = new Command();
        $tableHead = Utils::DataToTableHeader($sampleCommand);
        

        /* @var $command Command */
        foreach ($commands as $command) {
            //$tableData .= Utils::DataToTableFormat(array($command->CommandId, $command->Type, $command->Mode, $command->Repeat, $command->NextCommandId, $command->Timeout, Utils::ListToString($command->Groups), Utils::MapToString($command->Params)));
            $tableData .= Utils::DataToTableFormatAuto($command);
            
        }
        
        
        ?>

        <?php include 'MPBody1.php'; ?>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Commands</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Commands
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <?php
                                                    //echo "<tr><th>CommandId</th><th>Type</th><th>Mode</th><th>Repeat</th><th>NextCommandId</th><th>Timeout</th><th>Groups</th><th>Params</th></tr>";
                                                    echo $tableHead;
                                                ?>
                                                
                                            </thead>
                                            <tbody>
                                                <?php echo $tableData; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <?php include 'MPBody2.php'; ?>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').DataTable({
                    responsive: true,
                    "pageLength": 100
                });


            });
        </script>
    </body>
</html>
