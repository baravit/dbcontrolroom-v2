<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php include 'MPHead.php'; ?>
    </head>
    <body>

        <?php
        include("../Code/DAL.php");
        $var = isset($_POST['subBtn']);
        if (isset($_POST['subBtn'])) {
            $groupStr = str_replace(" ", "", $_POST['groups']);
            $deviceStr = str_replace(" ", "", $_POST['devices']);
            $groupsArray = explode(",", $groupStr);
            $devicesArray = preg_split("/\\r\\n|\\r|\\n/", $deviceStr);

            if ($_FILES["fileUpload"]['size'] > 0) {
                $content = file_get_contents($_FILES['fileUpload']['tmp_name']);
                $content = str_replace("\r", "", $content);
                $content = str_replace(" ", "", $content);
                
                $contentArray = explode("\n", $content);
                foreach ($contentArray as $line) {
                    array_push($devicesArray, $line);
                }
            }
            $groupsArray = array_diff($groupsArray, [" "]);
            $devicesArray = array_diff($devicesArray, [" "]);
            
            $groupsArray = array_filter($groupsArray); 
            $devicesArray = array_filter($devicesArray);
            
            $devicesArray = array_unique($devicesArray);
            $groupsArray = array_unique($groupsArray);
            
            
            
            $dal = new DAL();
            $success = $dal->AddGroupsToDevices($devicesArray, $groupsArray);

            if(Utils::IsTrue($success)) {
                header( "Location: Success.php" );
            } else {
                session_start();
                $_SESSION["Error"] = $success;
                header( "Location: Error.php" );
            }
        }
        ?>
        <?php include 'MPBody1.php'; ?>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Forms</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div style="margin-left: 20px; margin-right: 20px;" class="panel panel-default">
                    <div class="panel-heading">
                        Add groups to devices
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form action="AddGroup.php" role="form" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Choose Devices</label>
                                        <input name="fileUpload" id="fileUpload" type="file">
                                        <p class="help-block">Upload file or...</p>
                                        <textarea name="devices" id="devices" class="form-control" rows="3" placeholder="eg: 123,124,125..."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Choose Groups</label>
                                        <textarea name="groups" id="groups" class="form-control" rows="3" placeholder="eg: g1,g12..."></textarea>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Groups in DB
                                        </div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Groups</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $test;
                                                    try {
                                                        $dal = new DAL();
                                                        foreach ($dal->GetAllGroups() as $grp) {
                                                            $rows .= Utils::DataToTableFormat(array($grp));
                                                        }
                                                    } catch (Exception $exc) {
                                                        echo $exc->getTraceAsString();
                                                    }

                                                    echo $rows;
                                                    ?>

                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel panel-body -->
                                    <button id='subBtn' name='subBtn' type='submit' class="btn btn-success">Execute</button>
                                    <button type="reset" class="btn btn-info">Reset Button</button>
                                </form>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <?php include 'MPBody2.php'; ?>

        <script>
            $(document).ready(function () {
                $('#dataTables-example').DataTable({
                    responsive: true
                });

                $('#dataTables-example tbody').on('click', 'tr', function () {
                    $(this).toggleClass('selected');
                    var info = $(this).first().text(); // getting the selected row info
                    info = info.replace(/\s+/g, ''); // removing white spaces
                    info = info.replace(/(\r\n|\n|\r)/gm, ""); // removing line breakers
                    var add = true;
                    var finalContent = '';
                    var allgroups = $("#groups").val().split(",");
                    for (var i = 0; i < allgroups.length; i++) {
                        if (allgroups[i] != info) {
                            if (allgroups[i] != "")
                                finalContent += allgroups[i] + ",";
                        } else
                            add = false;
                    }
                    if (add)
                        finalContent += info + ",";

                    $("#groups").val(finalContent);
                });

            });
        </script>

    </body>
</html>
