<?php
    include_once '../Code/DAL.php';
    
    parse_str($_SERVER["QUERY_STRING"], $query_array);
    
    $dal = new DAL();
    /* @var $com Command */
    $com = $dal->GetCommandById($query_array["id"]);
    if($com == FALSE) {
        echo '<p style="color:red">Command:'.$query_array["id"].' could not be found</p>';
        flush();
        return;
    }
?>

<div class="form-group">
    <label>Command Id (auto generated)</label>
    <input id="commandId" name="commandId" class="form-control" readonly value='<?php echo $com->CommandId ?>'>
    <button type="Button" id="copyButton">Copy</button><br><br>
</div>
<div class="form-group">
    <label>Command Name</label>
    <input id="commandName" name="commandName" class="form-control" placeholder="ex: Steven" value ="<?php echo $com->CommandName ?>" >
</div>
<div class="form-group">
    <label>Type</label>
<!--    <select id="type" name="type" class="form-control">
        <option selected="selected">Choose a type or enter one in the text below</option>
        <option>Download</option>
        <option>Install</option>
        <option>Delete</option>
        <option>Start</option>
        <option>Stop</option>
        <option>RunCommand</option>
        <option>RemoveCommand</option>
        <option>UpdatePartameter</option>
        <option>UpdateMiner</option>
    </select>-->
    <input id="typeText" name="typeText" class="form-control" placeholder="Custom type" readonly value="<?php echo $com->Type ?>">
</div>
<div class="form-group">
    <label>Mode</label>
<!--    <select id="mode" name="mode" class="form-control">
        <option selected="selected">Choose a Mode or enter one in the text below</option>
        <option>Main</option>
        <option>Sub</option>
    </select>-->
    <input id="modeText" name="modeText" class="form-control" placeholder="Custom mode" readonly value="<?php echo $com->Mode ?>">
</div>
<div class="form-group">
    <label>Repeat</label>
    <input id="repeat" name="repeat" class="form-control" value="<?php echo $com->Repeat ?>" placeholder="The time between command execution in milliseconds. Use -1 for one time execution">
</div>
<div class="form-group">
    <label>Timeout</label>
    <input id="timeout" name="timeout" class="form-control" value="<?php echo $com->Timeout ?>" placeholder="The time limit for the command execution in milliseconds" >
</div>
<div class="form-group">
    <label>FailurePostpone</label>
    <input id="timeout" name="failurePostpone" class="form-control" value="<?php echo $com->FailurePostpone ?>" placeholder="The cooldown time when a command fails in milliseconds" >
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        Command Parameters
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example2">
            <thead>
                <tr><th>Name</th><th>Value</th></tr>
            </thead>
            <tbody>
                <?php 
                $counter = 0;
                foreach ($com->Params as $key => $value) {
                    echo '<tr>';
                    echo "<td><input name=\"paramsN" . $counter . "\" readonly class=\"from-control\" value=\"".key($value)."\" onkeyup=\"Expand(this);\"></td>";
                    echo "<td><input name=\"paramsV" . $counter .  "\" readonly class=\"from-control\" value=\"".$value[key($value)]."\" onkeyup=\"Expand(this);\"</td>";
                    echo '</tr>';
                    $counter++;
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel panel-body -->
<div class="form-group">
    <label>Next Command Id (optional)</label>
    <input id="nextCommandId" name="nextCommandId" readonly value="<?php echo $com->NextCommandId ?>" class="form-control">
</div>
<div class="form-group">
    <label>Choose Groups</label>
    <textarea name="groups" id="groups" class="form-control" rows="3" placeholder="eg: g1,g12,g123..."><?php echo Utils::ListToString($com->Groups, FALSE); ?></textarea>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        Groups in DB
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr><th>Group Name</th></tr>
            </thead>
            <tbody>
                <?php
                try {
                    include_once '../Code/DAL.php';
                    $dal = new DAL();
                    foreach ($dal->GetAllGroups() as $grp) {
                        $ExtraCalsses = "";
                        if(in_array($grp, $com->Groups)) {
                            $ExtraCalsses = "selected";
                        }
                        $ret = "<tr class=";
                        if (Utils::$IsEvenRow) {
                            $ret .= "\"even gradeX " . $ExtraCalsses . "\"";
                        } else {
                            $ret .= "\"odd gradeX\"";
                        }
                        $ret .= ">";
                        $ret .= "<td class=\"center lol\">" . $grp . "</td>";
                        $ret .= "</tr>";
                        Utils::$IsEvenRow = !Utils::$IsEvenRow;
                        $rows .= $ret;
                    }
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }

                echo $rows;
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel panel-body -->

<div class="form-group">
    <label>Comments</label>
    <textarea id="comments" name="comments" rows="3" class="form-control"><?php echo $com->Comments ?></textarea>
</div>

<!--<asp:Button id="button1" runat="server" Text="Execute" CssClass="btn btn-default" OnClick="SubmitButton" />-->
<button type="submit" name="subBtn" id="subBtn" class="btn btn-success">Submit Button</button>
<button type="reset" class="btn btn-info">Reset Button</button>