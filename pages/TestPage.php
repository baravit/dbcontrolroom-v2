<html>
    <head>
        <?php include 'MPHead.php'; ?>
    </head>
    <body>
        <?php include 'MPBody1.php'; ?>
        
        <p id="abc" name="abc">1</p>
        
        <?php include 'MPBody2.php'; ?>

        <script>
            $(document).ready(function () {
                setInterval(function(){
                    var xmlHttp = new XMLHttpRequest();
                    xmlHttp.open("GET", 'test.php', false); // false for synchronous request
                    xmlHttp.send(null);
                    $('#abc').html(xmlHttp.responseText);
                }, 1000);
            });
        </script>

    </body>
</html>
