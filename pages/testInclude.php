<?php
include '../Code/DAL.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 */

function file_get_php_classes($filepath) {
    $php_code = file_get_contents($filepath);
    $classes = get_php_classes($php_code);
    return $classes;
}

function get_php_classes($php_code) {
    $classes = array();
    $tokens = token_get_all($php_code);
    $count = count($tokens);
    for ($i = 2; $i < $count; $i++) {
        if ($tokens[$i - 2][0] == T_CLASS && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING) {

            $class_name = $tokens[$i][1];
            $classes[] = $class_name;
        }
    }
    return $classes;
}

try {
    $var = file_get_php_classes("../Code/DAL.php"); 
    
    if (!class_exists("DAL")) {
        $dal = new DAL();
    }

///echo count($dal->GetAllGroups());

    echo '$@end';
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}
?>